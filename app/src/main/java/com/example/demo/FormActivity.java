package com.example.demo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.demo.utils.Const;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FormActivity extends AppCompatActivity {


    @BindView(R.id.etActName)
    EditText etActName;
    @BindView(R.id.imageViewName)
    ImageView imageViewName;
    @BindView(R.id.etActPhoneNumber)
    EditText etActPhoneNumber;
    @BindView(R.id.imageViewPhoneNumber)
    ImageView imageViewPhoneNumber;
    @BindView(R.id.etActEmail)
    EditText etActEmail;
    @BindView(R.id.imageViewEmail)
    ImageView imageViewEmail;
    @BindView(R.id.rbActMale)
    RadioButton rbActMale;
    @BindView(R.id.rbActFemale)
    RadioButton rbActFemale;
    @BindView(R.id.rgActGender)
    RadioGroup rgActGender;
    @BindView(R.id.chbActCricket)
    CheckBox chbActCricket;
    @BindView(R.id.chbActFootBall)
    CheckBox chbActFootBall;
    @BindView(R.id.chbActHockey)
    CheckBox chbActHockey;
    @BindView(R.id.btnActSubmit)
    Button btnActSubmit;
    @BindView(R.id.tvActDisplay)
    TextView tvActDisplay;

    ArrayList<UserModel> userList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.etActName)
    public void onEtActNameClicked() {
    }

    @OnClick(R.id.imageViewName)
    public void onImageViewNameClicked() {
    }

    @OnClick(R.id.etActPhoneNumber)
    public void onEtActPhoneNumberClicked() {
    }

    @OnClick(R.id.imageViewPhoneNumber)
    public void onImageViewPhoneNumberClicked() {
    }

    @OnClick(R.id.etActEmail)
    public void onEtActEmailClicked() {
    }

    @OnClick(R.id.imageViewEmail)
    public void onImageViewEmailClicked() {
    }

    @OnClick(R.id.rbActMale)
    public void onRbActMaleClicked() {
    }

    @OnClick(R.id.rbActFemale)
    public void onRbActFemaleClicked() {
    }

    @OnClick(R.id.rgActGender)
    public void onRgActGenderClicked() {
    }

    @OnClick(R.id.chbActCricket)
    public void onChbActCricketClicked() {
    }

    @OnClick(R.id.chbActFootBall)
    public void onChbActFootBallClicked() {
    }

    @OnClick(R.id.chbActHockey)
    public void onChbActHockeyClicked() {
    }

    @OnClick(R.id.btnActSubmit)
    public void onBtnActSubmitClicked() {
        if(isValid()){

            String hobby="";

            UserModel userModel = new UserModel();

            if (chbActCricket.isChecked())
            {
                hobby += "," + chbActCricket.getText().toString();
            }
            if (chbActFootBall.isChecked())
            {
                hobby += "," + chbActFootBall.getText().toString();
            }
            if (chbActHockey.isChecked())
            {
                hobby += "," + chbActHockey.getText().toString();
            }

            userModel.setName(etActName.getText().toString());
            userModel.setPhonenumber(etActPhoneNumber.getText().toString());
            userModel.setEmail(etActEmail.getText().toString());
            userModel.setGender(rbActMale.isChecked()?"M":"F");
            userModel.setHobbies(hobby.substring(1));

            userList.add(userModel);

            Intent formIntent = new Intent(FormActivity.this, UserListActivity.class);
            formIntent.putExtra("UserList",userList);
            startActivity(formIntent);
        }
    }

    @OnClick(R.id.tvActDisplay)
    public void onTvActDisplayClicked() {
    }

    boolean isValid()
    {
        boolean flag = true;

        if (TextUtils.isEmpty(etActName.getText().toString()))
        {
            etActName.setError(getString(R.string.error_enter_value));
            flag = false;
        }


        if (TextUtils.isEmpty(etActPhoneNumber.getText().toString()))
        {
            etActPhoneNumber.setError(getString(R.string.error_enter_value));
            flag = false;
        }


        if (TextUtils.isEmpty(etActEmail.getText().toString()))
        {
            etActEmail.setError(getString(R.string.error_enter_value));
            flag = false;
        }
        return flag;

    }
}