package com.example.demo.utils;

public class Const {

    public static final String NAME = "Name";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String EMAIL = "Email";
    public static final String GENDER = "Gender";
    public static final String HOBBY = "Hobby";
}
