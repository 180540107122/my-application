package com.example.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.demo.adapter.UserListAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListActivity extends AppCompatActivity {

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter userListAdapter;
    @BindView(R.id.lvActUserList)
    ListView lvUsers;
    UserModel userModel = new UserModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        bindValues();
    }

    void bindValues() {
        userList.addAll((Collection<? extends UserModel>) getIntent().getSerializableExtra("UserList"));
        //userList.addAll((Collection<? extends UserModel>) getIntent().getSerializableExtra("UserList"));

        userListAdapter = new UserListAdapter(this, userList);
        lvUsers.setAdapter(userListAdapter);

        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                userModel = (UserModel) lvUsers.getItemAtPosition(position);

                Intent userDetailsIntent = new Intent(UserListActivity.this,UserDetails.class);
                userDetailsIntent.putExtra("User",userModel);
                startActivity(userDetailsIntent);
            }
        });


    }
}