package com.example.demo;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserDetails extends AppCompatActivity {

    @BindView(R.id.tvDetailGender)
    TextView tvDetailGender;
    @BindView(R.id.tvDetailName)
    TextView tvDetailName;
    @BindView(R.id.tvDetailPhoneNumber)
    TextView tvDetailPhoneNumber;
    @BindView(R.id.tvDetailEmail)
    TextView tvDetailEmail;
    @BindView(R.id.tvDetailHobbies)
    TextView tvDetailHobbies;

    UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_details);
        ButterKnife.bind(this);
        bindValues();
    }

    void bindValues() {
        userModel = (UserModel) getIntent().getSerializableExtra("User");

        tvDetailName.setText(userModel.getName());
        tvDetailPhoneNumber.setText(userModel.getPhonenumber());
        tvDetailEmail.setText(userModel.getEmail());
        tvDetailGender.setText(userModel.getGender());
        tvDetailHobbies.setText(userModel.getHobbies());
    }
}